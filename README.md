<div align="middle">

# Final Year Project 2021/2022
COLOR DETECTION AND IDENTIFICATION FOR UAV APPLICATIONS USING PYTHON

</div>

## Workflow
<div align="middle">
<img src="https://media.geeksforgeeks.org/wp-content/uploads/20200405135639/workflow.png" width="800">
</div>

## Progress
- [ ] find journals and identify points to be cited
- [x] imgmat.py (template matching with still image)
- [ ] test.py (template matching with live camera feed)
- [x] color.py (filter colours in live camera feed)
- [x] maskslider.py (filter colours in still image with adjustable sliders)
- [ ] test1.py (filter colours in live camera feed with adjustable sliders)
- [x] test3.py (multiple colour detection with labelling)
- [x] trial4.py (detecting per pixel rgb values with live camera feed)
