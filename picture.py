import cv2

cam = cv2.VideoCapture(0)
cv2.namedWindow("display")
img_counter = 0

while True:
    ret, frame = cam.read()
    if not ret:
        print("failed to grab frame")
        break
    
    cv2.imshow("display", frame)
    k = cv2.waitKey(1)
    if k%256 == 32:
        # SPACE pressed
        img_name = "frame_{}.png".format(img_counter)
        cv2.imwrite(img_name, frame)
        print("{} written!".format(img_name))
        img_counter += 1
    elif cv2.waitKey(1) == ord('q'):
        break

cam.release()
cv2.destroyAllWindows()
