from vidgear.gears import NetGear
import cv2

# Open suitable video stream, such as webcam on first index(i.e. 0)
stream = cv2.VideoCapture(0, cv2.CAP_V4L2)

# define tweak flags
options = {"flag": 0, "copy": False, "track": False}

# Define Netgear Client at given IP address and define parameters 
client = NetGear(
    address="119.40.125.128", port="5454")

while True:
    try:
        # read frames from stream
        (grabbed, frame) = stream.read()
        # check for frame if not grabbed
        if not grabbed:
            break
        # send frame to server
        client.send(frame)
    except KeyboardInterrupt:
        break

# safely close video stream
stream.release()
# safely close server
client.close()
