import cv2

video = cv2.VideoCapture(0, cv2.CAP_V4L2)
frame_width = int(video.get(3))
frame_height = int(video.get(4))
size = (frame_width, frame_height)
img_counter = 0
result = cv2.VideoWriter('video_{}.avi'.format(img_counter), cv2.VideoWriter_fourcc(*'MJPG'), 10, size)
    
while(True):
    ret, frame = video.read()
    if ret == True: 
        result.write(frame)
        cv2.imshow('Frame', frame)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
    else:
        break
  
video.release()
result.release()
cv2.destroyAllWindows()
