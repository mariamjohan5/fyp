import cv2
import socket
import pickle
import struct

cap=cv2.VideoCapture(0, cv2.CAP_V4L2)
clientsocket=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
clientsocket.connect(('localhost',8089))

while True:
    ret,frame=cap.read()
    # Serialize frame
    data = pickle.dumps(frame)
    # Send message length first
    message_size = struct.pack("L", len(data))
    # Then data
    clientsocket.sendall(message_size + data)
    if cv2.waitKey(1) == ord('q'):
        break
frame.release()